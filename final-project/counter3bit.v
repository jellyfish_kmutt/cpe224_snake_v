`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:46:50 04/28/2015 
// Design Name: 
// Module Name:    counter3bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module counter3bit(	input clk480,
							input reset,
							output reg [2:0] bitOut
    );
	 
	 always @(posedge clk480 or negedge reset) begin
		if (!reset) begin
			bitOut = 3'b000;
		end
		else begin
			if (bitOut == 3'b111) begin
				bitOut = bitOut + 1;
			end
			else begin
				bitOut = 0;
			end
		end
	 end
endmodule
