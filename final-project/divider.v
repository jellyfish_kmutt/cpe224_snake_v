`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:02:10 04/28/2015 
// Design Name: 
// Module Name:    divider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module divider(
    input clkin,
    input reset,
    output reg clkout
    );
	 reg[16:0]delay;
	 
	 always @(posedge clkin or negedge reset)
		if(!reset) 
			delay<=0; 
		else begin
			if(delay == 51999) begin
				delay<=0;
				clkout<=1'b1;
			end
			else begin
				delay<=delay+1;
				clkout<=1'b0;
			end
		end	
endmodule
