`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:39:37 04/28/2015 
// Design Name: 
// Module Name:    rowscan 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rowscan (	input [2:0] bitIn,
						output reg [7:0] col_en
						);
	
	wire [7:0] decodedData;
	decoder3to8 d2(bitIn,decodedData);
	always @(bitIn) begin
		col_en = decodedData;
	end
	
endmodule
