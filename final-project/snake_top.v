`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    14:56:20 04/23/2015
// Design Name:
// Module Name:    snake_top
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module snake_top(	input sysclk,
						input reset,
						input btn_up,
						input btn_down,
						input btn_left,
						input btn_right,
						output [7:0] col,
						output [7:0] row
						);

	wire clkOut_480;
	wire [2:0] count3bitOut;
	wire food_gen;
	wire [5:0] food;
	wire [5:0] snake;
	wire [2:0] LocSX;
	wire [2:0] LocSY;
	wire gameOver;
	wire icon_tick;
	wire ctrl_off;
	wire snakeLength;
	wire [2:0] loc_snake_col;
	wire [2:0] loc_snake_row;
	wire [7:0] column_en;
	wire [7:0] snake_col_data;
	wire [7:0] food_col_data;
	wire [7:0] coldatatodisp;

	divider div0(.clkin(sysclk),
				  .reset(reset),
				  .clkout(clkOut_480));

	counter3bit count0(.clk480(clkout),
						  .reset(reset),
						  .bitOut(count3bitOut));
						  
	rowscan rs(.bitIn(count3bitOut),
				  .col_en(column_en));
	
	snake_movement SM0(.clk(sysclk),
						  .reset(reset),
						  .game_over(gameOver),
						  .db_btn_west(btn_left),
						  .db_btn_east(btn_right),
						  .db_btn_north(btn_up),
						  .db_btn_south(btn_down),
						  .LocSX(loc_snake_col),
						  .LocSY(loc_snake_row),
						  .icon_tick(icon_tick),
						  .ctrl_off(ctrl_off),
						  .s_len(snakeLength)
						 );
						 
	snake_icon icon0( .whichrow(count3bitOut),
							.food_pos(food),
							.LocX_reg(loc_snake_col),
							.LocY_reg(loc_snake_row),
							.clk(sysclk),
							.reset(reset),
							.icon_tick(icon_tick),
							.s_len(snakeLength),
							.game_over(game_over),
							.food_gen(food_gen),
							.snakeLoc(snake_col_data),
							.foodLoc(food_col_data),
							.columnData(coldatatodisp)
						);

	snake_food SF0(.rand_enable(food_gen),
					 .foodPos(food));

endmodule
