`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:45:32 04/27/2015 
// Design Name: 
// Module Name:    snake_matrix 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module snake_matrix( input [2:0] whichrow,
							input [5:0] food_pos,
							input [5:0] snake_pos[0:63],
							output reg [2:0] out
    );
	 reg [2:0] snake_pos_col;
	 reg [2:0] food_pos_col;
	 reg [7:0] snake_disp;
	 reg [7:0] food_disp;
	 reg i = 0;
	 
	 always@(whichrow) begin
		for (i=0;i<64;i=i+1) begin
			if (snake_pos[i][2:0] == whichrow) begin
				snake_pos_col <= snake_pos[i][5:3];
				decoder3to8 d0(snake_pos_col, snake_disp);
			end
			if (food_pos[2:0] == whichrow) begin
				food_pos_col <= food_pos[5:3];
				decoder3to8 d0(food_pos_col, food_disp);
			end
		end
		
	 end


endmodule
