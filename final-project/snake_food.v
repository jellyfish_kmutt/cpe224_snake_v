`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    14:57:32 04/26/2015
// Design Name:
// Module Name:    snake-food
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module snake_food(input rand_enable,
						output reg [5:0] foodPos
						);

	reg [2:0] locCol_reg = 3'b101;
	reg [2:0] locRow_reg = 3'b101;
	reg [2:0] x,y;

	/*start assign the rand locations to the food*/
	always@(rand_enable) begin
		locCol_reg = ((x + 4271)*4273 - 9973*3)*57;
		locRow_reg = ((y + 3343)*3347 - 9857*3)*55;
		x = locCol_reg;
		y = locRow_reg;
		if (rand_enable == 1) begin
			foodPos <= {locCol_reg,locRow_reg};
		end
	end
	/*end assign the rand locations to the food*/
	
endmodule
