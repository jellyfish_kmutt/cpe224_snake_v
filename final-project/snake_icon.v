`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    14:52:19 04/23/2015
// Design Name:
// Module Name:    snake-icon
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module snake_icon(input [2:0] whichrow,
						input [5:0] food_pos,
						input [2:0] LocX_reg,
						input [2:0] LocY_reg,
						input clk,
						input reset,
						input icon_tick,
						input ctrl_off,
						output reg [2:0] icon,
						output reg [7:0]s_len = 8'd10,
						output reg game_over = 0,
						output reg food_gen = 0,
						output reg [7:0] snakeLoc,
						output reg [7:0] foodLoc,
						output reg [7:0] columnData
						);
	
	reg [5:0] head;
	reg [2:0] head_X;
	reg [2:0] head_Y;
	reg [7:0] i,j;
	reg [5:0] body_stack [63:0];
	reg [2:0] body_stack_col;
	reg [2:0] foodLoc_col;
	wire [7:0] snake_disp;
	wire [7:0] food_disp;
	reg k = 0;
	reg [2:0] Food_locX;
	reg [2:0] Food_locY;
	
	decoder3to8 d0(body_stack_col, snake_disp);
	decoder3to8 d1(foodLoc_col, food_disp);

	always@(whichrow) begin
		for (k=0;k<64;k=k+1) begin
			if (body_stack[k][2:0] == whichrow) begin
				body_stack_col = body_stack[k][5:3];
				snakeLoc = snake_disp;
				columnData = snakeLoc;
			end
			if (food_pos[2:0] == whichrow) begin
				foodLoc_col = food_pos[5:3];
				foodLoc = food_disp;
				columnData = foodLoc;
			end
		end		
	end

	always@(food_pos) begin
		Food_locX = food_pos[5:3];
		Food_locY = food_pos[2:0];
	end

	always @(posedge icon_tick or posedge reset) begin
		if( reset == 1 ) begin
			game_over = 0;		// Set the game over flag to zero on reset
			s_len = 8'd3;		//Intial length of the snake is '3'
			
			/*Initialize the bodystack to some initial values and set all others to zero on reset*/
			/*Start initialization*/
			for ( i=0;i<=63;i=i+1 ) begin
				body_stack[i] = 0;
			end
				body_stack[2] = {3'b000,3'b000};
				body_stack[1] = {3'b001,3'b000};
				body_stack[0] = {3'b010,3'b000};
		end
		/*End initialization*/

		else begin
			/*Capture the head locations of the snake the head_X,head_Y have been declared only for the better readability of the code*/
			head_X = LocX_reg;
			head_Y = LocY_reg;

		/*Start generation of the body dynamically while the snake eats more food*/
		for(i=63;i>=1;i=i-1) begin
			if(i <= s_len && ctrl_off == 0) begin //ctrl_off assures that the game is not yet over
				body_stack[i] = body_stack[i-1];
			end
			else begin
				body_stack[i] = 6'b000000;
				game_over = 0;
			end
		end
		/*End generation of the body dynamically while the snake eats more food*/

		if(ctrl_off == 0) //Check if the game is over
			body_stack[0] = {head_X,head_Y};
		else
			body_stack[0] = 0;

		/*Check if the generated food location is with in the region of the boundaries else regenerate the food*/
		if (ctrl_off ==0 &&(Food_locX < 3'b111 || Food_locY < 3'b111 || Food_locX > 3'b000 || Food_locY > 3'b000)) 
		begin
			food_gen  = 1;
		end
		else begin
			food_gen  = 0;
		end

		if(ctrl_off == 0 && body_stack[0]=={Food_locX,Food_locY}) 
		begin
			//Check if the head location of the snake is equal to the location of the food
			food_gen = 1; //If the above condition is met the food_gen is set high to generate the new location for the food
			/*The levels of the snake game are based on the updating length of the snake !*/
			if( s_len == 8'd8 ) /*Contion to jump from lvl - 1  to lvl - 2*/
				s_len = s_len + 8'd5;
			else if(s_len == 14) /*Contion to jump from lvl - 2  to lvl - 3*/
				s_len = s_len + 8'd10;
			else if( s_len == 25) /*Contion to jump from lvl - 3  to You Won !*/
				s_len = s_len + 8'd15;
			else
				s_len = s_len + 8'd1; /*When the snake is in a particular level its body increments by 1 when it eats food */
		end 
		else begin
			food_gen = 0;
		end

		/*Start check for game over*/
		for(j=63;j>=1;j=j-1) begin
			if( (body_stack[0] == body_stack[j]) &&(j <= s_len))begin /*Check the condition 1 - If the head location is equal to any of the body locations the game is over*/
				game_over = 1;
			end
			else begin /*This else has been used just to maintain a flip-flop kind of hardware instead of a latch*/
				j=j;
			end
		end

		if (s_len >= 8'd40)
			game_over = 1;
		/*End check for game over*/

		end
	end
endmodule
