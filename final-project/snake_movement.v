`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    15:05:52 04/23/2015
// Design Name:
// Module Name:    snake_movement
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module snake_movement(	input clk,
								input reset,
								input game_over,
								input db_btn_west,
								input db_btn_east,
								input db_btn_north,
								input db_btn_south,
								output [2:0] LocSX,
								output [2:0] LocSY,
								input [7:0] s_len,
								output reg icon_tick = 0,
								output reg ctrl_off = 0,
								output reg [5:0] snakeLocaction
								);

	/* Different parameters to select the speeds for the levels */
	localparam snake_cnt1 = 28'd9_999_999;
	localparam snake_cnt2 = 28'd5_999_999;
	localparam snake_cnt3 = 28'd1_999_999;

	reg [2:0] LocSX_reg ,LocSY_reg;
	reg [3:0] current_heading,user_inputs; //Keeps track of the user inputs and current heading direction of thesnake
	reg [28:0] tick_inc = 28'h0;
	reg [28:0] snake_speed = snake_cnt1; // register to store the speed with which the snake is moving
	reg [31:0]k; //Register to handle meta-stability

	always @(posedge clk)begin
		if (tick_inc == snake_speed) begin
			 icon_tick <= 1;
			 tick_inc <= 0;
		end
		else begin
			 tick_inc <= tick_inc + 1;
			 icon_tick <= 0;
		end
	 end

	/*start user input*/
	/*Capture the debounced outputs from the switch
	Notice here if the ctrl_off flag is high pressing the buttons
	wont effect the movement of the snake
	*/
	always @(*) begin
		if(db_btn_west == 1 && ctrl_off == 0)begin
			user_inputs = 4'b0001;
		end
		else if(db_btn_east == 1 && ctrl_off == 0)begin
			user_inputs = 4'b0010;
		end
		else if(db_btn_north == 1 && ctrl_off == 0 )begin
			user_inputs = 4'b0100;
		end
		else if(db_btn_south == 1 && ctrl_off == 0)begin
			user_inputs = 4'b1000;
		end
	end
	/*end user input*/

	always @(posedge icon_tick) begin
		/*Start level check*/
		/*Since level of the snake determines the level depending on the length of the snake the
		clock speed is changed and hence this will reflect on the screen by making the snake move faster
		*/
		if(s_len >= 8'd6 && s_len <= 8'd8)begin
		snake_speed = snake_cnt1;
		end
		else if(s_len >= 8'd13 && s_len <= 8'd14) begin
		snake_speed = snake_cnt2;
		end
		else begin
		snake_speed = snake_cnt3;
		end
		/*End level check*/

		/*Start Basic movement*/
		/*The first condition here checks here if the snake is moving east and if we press the east
		or west button it will have no effect on the snakes movement.Similarly for other directions*/
		if ( user_inputs == 4'b0010 && current_heading != 4'b0001 && current_heading != 4'b0010)begin
		current_heading = 4'b0010;
		end
		else if ( user_inputs == 4'b0001 && current_heading != 4'b0001 && current_heading != 4'b0010)begin
		current_heading = 4'b0001;
		end
		else if ( user_inputs == 4'b0100 && current_heading != 4'b0100 && current_heading != 4'b1000)begin
		current_heading = 4'b0100;
		end
		else if ( user_inputs == 4'b1000 && current_heading != 4'b0100 && current_heading != 4'b1000)begin
		current_heading = 4'b1000;
		end
		/*End Basic movement*/

		if(reset) begin
			/*When reset is pressed the locations will be changed to default locations*/
			LocSX_reg = 3'b010;
			LocSY_reg = 3'b000;
		end
		else begin
			case(current_heading)
			/*Depending on the current heading directions the locations will continuously
			increment or decrement.If the snake was moving east and if non of the
			north or south button are pressed X - location will be incremented.
			Similarly for other directions.
			*/
			4'b0001: LocSX_reg = LocSX_reg - 1;
			4'b0010: LocSX_reg = LocSX_reg + 1;
			4'b0100: LocSY_reg = LocSY_reg - 1;
			4'b1000: LocSY_reg = LocSY_reg + 1;
			endcase
		end
	end
	/*Start Handle initial Metastability.
	This block will handle the meta-stability which arises initially
	for getting into game over condition.The offset value for the
	meta-stability has been based purely on observation.The code
	is running perfectly for k > 1.
	*/
	always @ (posedge game_over or posedge reset) begin
		if (reset) begin
			ctrl_off = 0;
			k = 0;
		end else begin
			k = k + 1;
			if(game_over && (k > 1))
			ctrl_off = 1;
		end
		
	end
	
	/*End Handle intial Metastability*/
	//Finally the head locations are assigned to the LocSX and LocSY
	assign LocSX = LocSX_reg;
	assign LocSY = LocSY_reg;
	//snakeLocaction = {LocSX_reg,LocSY_reg};

endmodule
