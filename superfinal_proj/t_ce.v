`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:36:08 04/30/2015
// Design Name:   check_eat
// Module Name:   D:/test/t_ce.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: check_eat
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_ce;

	// Inputs
	reg [383:0] head_loc;
	reg clk;
	reg rst;

	// Outputs
	wire [5:0] foodPos;
	wire gen_food;
	wire increase_length;

	// Instantiate the Unit Under Test (UUT)
	check_eat uut (
		.head_loc(head_loc), 
		.clk(clk), 
		.rst(rst), 
		.foodPos(foodPos), 
		.increase_length(increase_length)
	);

	initial begin
		// Initialize Inputs
		head_loc = 0;
		clk = 0;
		rst = 1;

		// Wait 100 ns for global reset to finish
		#100;
      rst = 0;
		#100;
      rst = 1;
		// Add stimulus here
		repeat (1000) begin 
			#5 clk =~clk;
			#100 head_loc = foodPos;
			#100 head_loc = 0; #100 head_loc = 0;
			#100 head_loc = foodPos;
			#100 head_loc = foodPos; #100 head_loc = 0;
			#100 head_loc = foodPos;
			
			
			
			
			
			
		end

	end
	initial #300 $finish;
      
endmodule

