`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:02:10 04/26/2015 
// Design Name: 
// Module Name:    divider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module divider(
    input clkin,
    input reset,
    output reg clkout
    );
	 
	 reg[25:0]delay;	 
	 always @(posedge clkin or negedge reset)
		if(!reset) begin		
			delay<=0;
			clkout<=0;	
		end
		else begin
			if(delay == 4_999_999) begin
				delay<=0;
				clkout<=~clkout;
			end
			else begin
				delay<=delay+1;
			end
		end	
endmodule
