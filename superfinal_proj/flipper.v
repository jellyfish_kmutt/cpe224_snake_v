`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:10:54 04/30/2015 
// Design Name: 
// Module Name:    flipper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module flipper( input [7:0] dataIn,
					 input rst,
					 input clk,
					 output reg [7:0] flipped
    );

	always@(dataIn) begin
//	
//		if(!rst) begin			
//			flipped <= 8'b0000_0000;
//			i<=0;
//		end
//		else begin 
//		if (i==7) begin
//				flipped[i]<=~dataIn[i];
//				i<=-1;
//			end
//			else if (i>=0) begin
//				flipped[i]<=~dataIn[i];
//				i<=i+1;
//			end
//		end
//		
//	end
	 flipped=~dataIn;
	end
endmodule
