`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   07:57:10 04/30/2015
// Design Name:   topModule
// Module Name:   D:/test/tf.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: topModule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module tf;

	// Inputs
	reg clock;
	reg reset;
	reg pbwest;
	reg pbnorth;
	reg pbeast;
	reg pbsouth;

	// Outputs
	wire [7:0] row_enable;
	wire [7:0] col_enable;

	// Instantiate the Unit Under Test (UUT)
	topModule uut (
		.clock(clock), 
		.reset(reset), 
		.pbwest(pbwest), 
		.pbnorth(pbnorth), 
		.pbeast(pbeast), 
		.pbsouth(pbsouth), 
		.row_enable(row_enable), 
		.col_enable(col_enable)
	);

	initial begin
		// Initialize Inputs
		clock = 0;
		reset = 0;
		pbwest = 0;
		pbnorth = 0;
		pbeast = 0;
		pbsouth = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
repeat (100)
		#10 clk=~clk;
		body_stack=6'b100101;
	end
      
endmodule

