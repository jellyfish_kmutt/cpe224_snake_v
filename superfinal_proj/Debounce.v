`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:04:29 05/01/2013 
// Design Name: 
// Module Name:    Debounce 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Debounce(noise,clk,debounced);
input wire clk,noise;
output debounced;

reg pb_sync_0;
always @(posedge clk)pb_sync_0 <= ~noise;
reg pb_sync_1;
always @(posedge clk)pb_sync_1 <= pb_sync_0;

reg [15:0] pb_cnt;
reg pb_state;
wire pb_idle = (pb_state==pb_sync_1);
wire pb_cnt_max  = &pb_cnt;

always @(posedge clk)
begin
 if(pb_idle)
    pb_cnt<=0;
 else
 begin
  pb_cnt <= pb_cnt+1;
  if(pb_cnt_max)pb_state<=~pb_state;
 end 
end

assign debounced = ~pb_state & ~pb_idle & pb_cnt_max;

endmodule
