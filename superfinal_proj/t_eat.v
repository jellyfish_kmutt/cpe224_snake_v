`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:37:42 04/30/2015
// Design Name:   check_eat
// Module Name:   D:/test/t_eat.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: check_eat
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_eat;

	// Inputs
	reg [5:0] food_pos;
	reg [383:0] head_loc;
	reg clk;

	// Outputs
	wire gen_food;
	wire increase_length;

	// Instantiate the Unit Under Test (UUT)
	check_eat uut (
		.food_pos(food_pos), 
		.head_loc(head_loc), 
		.clk(clk), 
		.gen_food(gen_food), 
		.increase_length(increase_length)
	);

	initial begin
		// Initialize Inputs
		food_pos = 0;
		head_loc = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		repeat (1000) begin
			#5 clk=~clk;
			#100 food_pos = 6'b101110; head_loc = 6'b101110;
			
			#100 food_pos = 6'b100010; head_loc = 6'b101010;
			#100 food_pos = 6'b100010; head_loc = 6'b101011;
			#100 food_pos = 6'b100010; head_loc = 6'b101110;
			
			#100 food_pos = 6'b001010; head_loc = 6'b001010;
		end

	end
      
endmodule

