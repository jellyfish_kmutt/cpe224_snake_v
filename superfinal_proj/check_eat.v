`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:39:33 04/27/2015 
// Design Name: 
// Module Name:    check_eat 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module check_eat( 
	input [383:0] head_loc,
	input [5:0] clk,
	input rst,
	output reg [5:0] foodPos,
	output reg increase_length
	);

	reg [2:0] x,y;
	reg [5:0] food_pos;
	
	always@(posedge clk[0]) begin
		if(!rst) begin
			x <= 3'b111;
			y <= 3'b101;
			food_pos <= {x,y};
			increase_length = 0;
		end
		else begin			
			if (head_loc[5:0] == food_pos[5:0]) begin
				x <= y + 4;
				y <= x + 8;
				food_pos <= {x,y};
				increase_length = 1;				
			end
			else begin
				x <= x;
				y <= y;
				food_pos <= {x,y};				
				increase_length = 0;
			end
		end
		foodPos = food_pos;
	end
		
endmodule
