`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:56:24 05/05/2015
// Design Name:   divider
// Module Name:   C:/Users/Sirijet/cpe224-snake/superfinal_proj/t_div.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: divider
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_div;

	// Inputs
	reg clkin;
	reg reset;

	// Outputs
	wire clkout;

	// Instantiate the Unit Under Test (UUT)
	divider uut (
		.clkin(clkin), 
		.reset(reset), 
		.clkout(clkout)
	);

	initial begin
		// Initialize Inputs
		clkin = 0;
		reset = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		reset = 1;
		repeat(25000000) #5 clkin = ~clkin;
		
		

	end
      
endmodule

