`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   05:01:21 04/30/2015
// Design Name:   displaymodule
// Module Name:   D:/test/t_display.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: displaymodule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_display;

	// Inputs
	reg [383:0] body_stack;
	reg clk;
	reg gameover;
	reg reset;

	// Outputs
	wire [7:0] row_enable;
	wire [7:0] col_enable;

	// Instantiate the Unit Under Test (UUT)
	displaymodule uut (
		.body_stack(body_stack), 
		.clk(clk), 
		.gameover(gameover), 
		.reset(reset), 
		.row_enable(row_enable), 
		.col_enable(col_enable)
	);

	initial begin
		// Initialize Inputs
		body_stack = 0;
		clk = 0;
		gameover = 0;
		reset = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		repeat (100)
		#10 clk=~clk;
		body_stack=6'b100101;
		
	end
      
endmodule

