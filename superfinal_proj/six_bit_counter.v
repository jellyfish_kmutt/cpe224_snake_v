`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:20:41 04/27/2015 
// Design Name: 
// Module Name:    six_bit_counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module six_bit_counter(
    input clk_in,
	input rst,
    output reg [5:0] q
    );
	 
	always@(negedge rst or posedge clk_in) begin
		if(!rst) q <= 6'b000000;
		else q <= q + 6'b000001;
	end

endmodule
