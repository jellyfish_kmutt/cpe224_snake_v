`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:44:51 04/29/2015
// Design Name:   snake_body
// Module Name:   D:/test/t_snake_body.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: snake_body
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_snake_body;

	// Inputs
	reg rst;
	reg clk;
	reg turn_up;
	reg turn_down;
	reg turn_left;
	reg turn_right;

	// Outputs
	wire [5:0] x0;
	wire [5:0] x1;
	wire [5:0] x2;

	// Instantiate the Unit Under Test (UUT)
	snake_body uut (
		.rst(rst), 
		.clk(clk),
		.turn_up(turn_up),
		.turn_down(turn_down),
		.turn_left(turn_left),
		.turn_right(turn_right),
		.x0(x0), 
		.x1(x1),
		.x2(x2)
	);

	initial begin
		rst = 0;
		#2 rst = 1;
		#3 rst = 0;
		clk =  0;
		turn_right = 0;
		turn_down = 0;
		turn_left = 0;
		turn_up = 1;
		#400 turn_up = 0;
		turn_left = 1;
		#400 turn_left = 0;
	end
	initial begin 
		repeat(1000) begin
			#2 clk = ~clk;
		end
	end
      
endmodule

