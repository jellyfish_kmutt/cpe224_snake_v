`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    14:57:32 04/26/2015
// Design Name:
// Module Name:    snake-food
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module rand_food(
	input rand_enable,
	input rst,
	output reg [5:0] foodPos
);

	reg [2:0] x,y;

	/*start assign the rand locations to the food*/
	always@(negedge rst or posedge rand_enable) begin
		if(!rst) begin
			x <= 3'b101;
			y <= 3'b101;
		end
		else begin
			x <= y + 2;
			y <= x + 5;
			if (rand_enable == 1) begin
				foodPos <= {x,y};
			end
			else begin
				x <= x;
				y <= y;
				foodPos <= {x,y};
			end
		end
	end
	/*end assign the rand locations to the food*/
	
endmodule
