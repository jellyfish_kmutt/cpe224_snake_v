`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   07:58:14 04/30/2015
// Design Name:   flipper
// Module Name:   D:/test/t_flip.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: flipper
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_flip;

	// Inputs
	reg [7:0] dataIn;
	reg rst;
	reg clk;

	// Outputs
	wire [7:0] flipped;

	// Instantiate the Unit Under Test (UUT)
	flipper uut (
		.dataIn(dataIn), 
		.rst(rst), 
		.clk(clk), 
		.flipped(flipped)
	);

	initial begin
		// Initialize Inputs
		dataIn = 0;
		rst = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100; rst = 1;
		// Add stimulus here
		repeat (1000) begin
			#5 clk=~clk; 
			#10; dataIn=8'b01010101;
			
		 end
		 
	end
      
endmodule

