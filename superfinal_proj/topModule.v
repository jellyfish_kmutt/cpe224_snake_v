`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:14:45 04/29/2015 
// Design Name: 
// Module Name:    topModule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module topModule(
	input clock,
	input reset,
	input pbwest,
	input pbnorth,
	input pbeast,
	input pbsouth,
	output [7:0] row_enable,
	output [7:0] col_enable 
	);
	
	wire food_gen,gameover;
	wire increase_length;
	wire [5:0] foodLoc;
	wire [5:0] index;
	wire [5:0] index_for_processing;
	wire [5:0] snake_len;
	wire [383:0] body_stack;
	wire clk_5Hz;
	
	divider 			DIV5Hz(clock, reset, clk_5Hz);
	six_bit_counter 	COUNT1(clock,reset,index);
	six_bit_counter 	COUNT2(clk_5Hz,reset,index_for_processing);
	check_eat 			EAT(body_stack, index_for_processing, reset, foodLoc, increase_length);		 
	snake_body 			BODY(reset, ~pbnorth, ~pbsouth, ~pbwest, ~pbeast, increase_length, 
						     index_for_processing, body_stack, gameover, snake_len);	
	displaymodule 		DISP(body_stack,foodLoc,gameover,reset,index,snake_len,row_enable,col_enable);
endmodule
