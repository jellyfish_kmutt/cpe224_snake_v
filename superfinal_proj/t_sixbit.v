`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:54:31 04/29/2015
// Design Name:   six_bit_counter
// Module Name:   D:/test/t_sixbit.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: six_bit_counter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_sixbit;

	// Inputs
	reg clk_in;
	reg rst;

	// Outputs
	wire [5:0] q;

	// Instantiate the Unit Under Test (UUT)
	six_bit_counter uut (
		.clk_in(clk_in), 
		.rst(rst), 
		.q(q)
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		rst = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rst=0;
		repeat (500)
		#2 clk_in=~clk_in;
	end
      
endmodule

