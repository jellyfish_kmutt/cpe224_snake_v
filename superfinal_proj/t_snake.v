`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:48:43 04/30/2015
// Design Name:   snake_body
// Module Name:   D:/test/t_snake.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: snake_body
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_snake;

	// Inputs
	reg rst;
	reg turn_up;
	reg turn_down;
	reg turn_left;
	reg turn_right;
	reg increase_length;
	reg [5:0] check_index;

	// Outputs
	wire [383:0] body_stack;
	wire gameover;
	wire [5:0] snake_len;

	// Instantiate the Unit Under Test (UUT)
	snake_body uut (
		.rst(rst), 
		.turn_up(turn_up), 
		.turn_down(turn_down), 
		.turn_left(turn_left), 
		.turn_right(turn_right), 
		.increase_length(increase_length), 
		.check_index(check_index), 
		.body_stack(body_stack), 
		.gameover(gameover), 
		.snake_len(snake_len)
	);

	initial begin
		// Initialize Inputs
		rst = 0;
		turn_up = 0;
		turn_down = 0;
		turn_left = 0;
		turn_right = 0;
		increase_length = 0;
		check_index = 0;

		// Wait 100 ns for global reset to finish
		#10 rst = 1;
		#2 rst = 0;
		// Add stimulus here

	end
	initial repeat(1000) #2 check_index = check_index + 1;
      
endmodule

