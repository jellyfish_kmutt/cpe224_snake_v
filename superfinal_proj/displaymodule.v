`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:53:26 04/27/2015 
// Design Name: 
// Module Name:    displaymodule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module displaymodule(
	input [383:0] body_stack,
	input [5:0] foodPos,
	input gameover,
	input rst,
	input [5:0] index,
	input [5:0] snake_len,
	output reg [7:0] row_enable,
	output reg [7:0] col_enable
	);
	
	reg [2:0] tempX, tempY;
	reg [5:0] wanted_coordinate;
	wire [7:0] testcol,testrow,foodCol,foodRow;
	
	decoder3to8 DEC1(tempX, testcol);
	decoder3to8 DEC2(tempY, testrow);
	decoder3to8 DEC3(foodPos[5:3],foodCol);
	decoder3to8 DEC4(foodPos[2:0],foodRow);
	
	always@(*) begin
		wanted_coordinate = body_stack >> (6 * index);
		tempX = wanted_coordinate[5:3];
		tempY = wanted_coordinate[2:0];
		if (!rst) begin
			row_enable <= ~(testrow);
			col_enable <= testcol;
		end
		else if (gameover == 1) begin
			row_enable <= 8'b0000_0000;
			col_enable <= 8'b1111_1111;
		end		
		else begin 
			if(index == 63) begin
				row_enable <= ~(foodRow);
				col_enable <= foodCol;
			end
			else if(index > snake_len) begin
				row_enable <= 8'b1111_1111;
				col_enable <= 8'b0000_0000;
			end
			else begin
				row_enable <= ~(testrow);
				col_enable <= testcol;
			end
		end
	end
endmodule
