`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:38:25 04/27/2015 
// Design Name: 
// Module Name:    snake_body 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module snake_body(
	input rst,
	input turn_up,
	input turn_down,
	input turn_left,
	input turn_right,
	input increase_length,
	input [5:0] check_index,
	output reg [383:0] body_stack, //64 index * (3 vertical-index bit + 3 horizontal-index bit) = 384
	output reg gameover,
	output reg[5:0] snake_len
	);
	
	reg[1:0] head_direction; //2'b00 upward, 2'b01 eastward, 2'b10 downward, 2'b11 westward
	reg[5:0] temp; //use as temperal coordinate register for collision dectector
	
	always@(negedge rst or posedge check_index[0]) begin
		if(!rst) begin
			gameover <= 0;
			snake_len <= 2;
			body_stack[5:0] <= 6'o03;
			body_stack[11:6] <= 6'o02;
			body_stack[17:12] <= 6'o01;
			head_direction <= 2'b01;
		end
		else begin //dynamically update snake body
			temp = body_stack >> (6 * check_index);
			if((check_index != 6'b000000) && (check_index < snake_len + 1) && (temp[5:0] == body_stack[5:0])) 
				gameover <= 1;
			//if (!gameover) begin
				if(increase_length) 
					snake_len <= snake_len + 1; //increase body length
				body_stack[383:6] <= body_stack[377:0];
				
				 //update heading direction
				case({head_direction[0], turn_up, turn_down, turn_left, turn_right}) //update head location
					6'b00010: begin //heading up or down, turn left
						body_stack[5:0] <= {body_stack[5:3], body_stack[2:0] - 3'b001};
						head_direction <= 2'b11;
					end
					6'b00001: begin //heading up or down, turn right
						body_stack[5:0] <= {body_stack[5:3], body_stack[2:0] + 3'b001};
						head_direction <= 2'b01;
					end
					6'b11000: begin //heading left or right, turn up
						body_stack[5:0] <= {body_stack[5:3] - 3'b001, body_stack[2:0]};
						head_direction <= 2'b00;
					end
					6'b10100: begin //heading left or right, turn down
						body_stack[5:0] <= {body_stack[5:3] + 3'b001, body_stack[2:0]};  
						head_direction <= 2'b10;
					end
					default: begin
						case(head_direction)
							2'b00: body_stack[5:0] <= {body_stack[5:3] - 3'b001, body_stack[2:0]}; //upward
							2'b01: body_stack[5:0] <= {body_stack[5:3], body_stack[2:0] + 3'b001}; //eastward
							2'b10: body_stack[5:0] <= {body_stack[5:3] + 3'b001, body_stack[2:0]}; //downward
							2'b11: body_stack[5:0] <= {body_stack[5:3], body_stack[2:0] - 3'b001}; //westward
						endcase
					end
				endcase
			//end			
		end
	end

endmodule
