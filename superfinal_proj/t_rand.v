`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:15:10 04/30/2015
// Design Name:   rand_food
// Module Name:   D:/test/t_rand.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: rand_food
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_rand;

	// Inputs
	reg rand_enable;
	reg rst;

	// Outputs
	wire [5:0] foodPos;

	// Instantiate the Unit Under Test (UUT)
	rand_food uut (
		.rand_enable(rand_enable), 
		.rst(rst), 
		.foodPos(foodPos)
	);

	initial begin
		// Initialize Inputs
		rand_enable = 0;
		rst = 1;

		// Wait 100 ns for global reset to finish
		
        
		// Add stimulus here
		#100 rst = 0;
		#100 rst = 1;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		#100 rand_enable = 0;
		#100 rand_enable = 1;
		
		
		

	end
      
endmodule

