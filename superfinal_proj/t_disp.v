`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:12:58 04/30/2015
// Design Name:   displaymodule
// Module Name:   D:/test/t_disp.v
// Project Name:  test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: displaymodule
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module t_disp;

	// Inputs
	reg [383:0] body_stack;
	reg clk;
	reg gameover;
	reg reset;
	reg [5:0] index;
	reg [5:0] snake_len;

	// Outputs
	wire [7:0] row_enable;
	wire [7:0] col_enable;

	// Instantiate the Unit Under Test (UUT)
	displaymodule uut (
		.body_stack(body_stack), 
		.clk(clk), 
		.gameover(gameover), 
		.reset(reset), 
		.index(index),
		.snake_len(snake_len),
		.row_enable(row_enable), 
		.col_enable(col_enable)
	);

	initial begin
		// Initialize Inputs
		body_stack = 384'o30_20_10_07_06_05_04_03_02_01_00;
		clk = 0;
		gameover = 0;
		reset = 0;
		index = 0;
		snake_len = 5;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
	initial begin
		#100 index = 1;
		#100 index = 2;
		#100 index = 3;
		#100 index = 8;
		#100 index = 9;
		#100 index = 63;
	end
		
      
endmodule

