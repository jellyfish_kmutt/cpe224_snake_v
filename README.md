# README #

## ARCADE SNAKE GAME ON 8x8 LED DOT-MATRIX DISPLAY USING VERILOG ##

### List of Modules ###
1. Frequency divider module (divider.v)
This module divides onboard-oscillator frequency 25MHz by 5 million to get the output frequency 5Hz.

2. Six-bit counter module (six_bit_counter.v)
The module counts six bits according to input frequency for using in another module.

3. Module for checking whether the snake eats food or not (check_eat.v) 
If it eats one, the module sends the signal to increase the snake length. This module also generates new food randomly when one is eaten.

4. Snake body module (snake_body.v)
This module focuses on snake’s location on dot-matrix LEDs.  It handles with snake direction according to user control received from push buttons. 

5. Module for display on dot-matrix LEDs (displaymodule.v)
The module receives 6-bit data from snake_body  and then it considers which dot the body is in, and check_eat module. And then it sends data to decoder to get 8-bit data and then sends to dot-matrix LEDs to display. 

6. 3-to-8 decoder module (decoder3to8.v)
The module receives the location of an object in six-bit pattern –  first three bits is for row and last three is for column. And then it decodes each three bits to eight bits for using to enable row and column.

7. Top module (topModule.v)
